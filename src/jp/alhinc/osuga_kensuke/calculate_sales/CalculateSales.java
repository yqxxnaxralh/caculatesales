package jp.alhinc.osuga_kensuke.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class CalculateSales {
	public static void main(String[] args) {
		//コマンドライン引数が渡されていない場合
		//コマンドライン引数が2つ以上の場合
		if (args.length == 0) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		if (args.length == 2) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//支店定義ファイルMap
		HashMap<String, String> sitenMap = new HashMap<String, String>();
		//売上初期値Map
		HashMap<String, Long> uriageOrigin = new HashMap<String, Long>();
		BufferedReader br = null;

		//支店定義ファイル取り込みメソッドの呼び出し
		boolean input = input(args, sitenMap, uriageOrigin, "branch.lst", "支店", "[0-9]{3}");
		if (input == false) {
			return;
		}

		//売り上げファイル名取得
		List<String> fileList = new LinkedList<String>();
		//ファイル名
		String fileName = null;
		try {
			File file = new File(args[0]);
			File[] files = file.listFiles();
			int a = 1;
			for (int i = 0; i < files.length; i++) {
				//ファイル確認
				if (files[i].isFile()) {
					fileName = files[i].getName();
					//売上ファイル連番チェックファイル名が連番最大値でないフォルダが含まれている場合
					if (fileName.matches("[0-9]{8}.rcd") == true) {
						int renban = Integer.parseInt(fileName.substring(0, fileName.length() - 4));
						if (renban == a++) {
							fileList.add(fileName);
						} else {
							//売上ファイル連番チェック 歯抜けになっている場合
							//売上ファイル連番チェック 連番最大値ではないフォルダーが含まれている場合
							//売上ファイル連番チェック ファイル名の末尾にゴミがついている場合
							//売上ファイル連番チェック ファイル名の先頭にゴミがついている場合
							System.out.println("売上ファイル名が連番になっていません");
							return;
						}
					}
				}
			}
			for (int i = 0; i < fileList.size(); i++) {
				File file1 = new File(args[0], fileList.get(i));
				FileReader fr = new FileReader(file1);
				br = new BufferedReader(fr);
				String line;
				List<String> uriageList = new LinkedList<String>();
				while ((line = br.readLine()) != null) {
					uriageList.add(line);
				}
				//売上ファイルの行数が1行以下の場合
				//売上ファイルの行数が3行以上の場合
				if (uriageList.size() != 2) {
					System.out.println(fileList.get(i) + "のフォーマットが不正です");
					return;
				}
				//売上金額 にひらがなが含まれている場合
				//売上金額 にカタカナが含まれている場合
				//売上金額 に漢字が含まれている場合
				//売上金額 に記号が含まれている場合
				if (uriageList.get(1).matches("^[0-9]+$") == false) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				//支店別売上合計計算
				if (!(uriageOrigin.containsKey(uriageList.get(0)))) {
					//売上ファイルの支店コードが支店定義ファイルに存在しない場合
					System.out.println(fileList.get(i) + "の支店コードが不正です");
					return;
				}
				long sum = uriageOrigin.get(uriageList.get(0)) + Long.parseLong(uriageList.get(1));
				//支店別集計合計金額が10桁超えた場合
				if (String.valueOf(sum).length() > 10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				uriageOrigin.put(uriageList.get(0), sum);
				uriageList.clear();
			}
			//ファイル出力メソッドの呼び出し
			boolean outFile = output(sitenMap, uriageOrigin, args[0], "branch.out");
			if (outFile != true) {
				return;
			} else {
				return;
			}
		} catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (!(br == null)) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
	}

	//支店定義ファイル取り込みメソッド
	private static boolean input(String[] args, HashMap<String, String> sitenMap, HashMap<String, Long> uriageOrigin,
			String fileName, String name, String conditions) {
		BufferedReader br = null;
		try {
			File file = new File(args[0], fileName);
			//支店定義ファイルが存在しない場合
			if (file.exists() == false) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				String[] number = line.split(",");
				//支店定義ファイルのフォーマット不正エラー
				//支店定義ファイル1行の要素数が 1つ以下の場合
				//支店定義ファイル1行の要素数が 3つ以上の場合
				if (!(number.length == 2)) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				//支店コード に数値以外の値
				//支店コードにアルファベットが含まれている場合
				//支店コードにひらがなが含まれている場合
				//支店コードにカタカナが含まれている場合
				//支店コードに漢字が含まれている場合
				//支店コードに記号が含まれている場合
				//支店コードの桁数が 2桁以下の場合
				//支店コードの桁数が 4桁以上の場合
				if (!(number[0].matches(conditions))) {
					System.out.println(name + "定義ファイルのフォーマットが不正です");
					return false;
				}
				sitenMap.put(number[0], number[1]);
				uriageOrigin.put(number[0], (long) 0);
			}
			return true;
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
	}

	//ファイル出力メソッド
	private static boolean output(HashMap<String, String> sitenMap, HashMap<String, Long> uriageOrigin, String hikisuu,
			String fileName) {
		PrintWriter pw = null;
		try {
			File newfile = new File(hikisuu, fileName);
			FileWriter fw = new FileWriter(newfile);
			pw = new PrintWriter(fw);
			for (String s : sitenMap.keySet()) {
				pw.println(s + "," + sitenMap.get(s) + "," + uriageOrigin.get(s));
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (!(pw == null)) {
				pw.close();
			}
		}
		return true;
	}
}